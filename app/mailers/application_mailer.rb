class ApplicationMailer < ActionMailer::Base
  default from: 'jedis@test.com'
  layout 'mailer'

  def welcome(resident)
    @user = resident
    mail(to: @user.email, subject: 'Confirmação de Cadastro')
  end
end
